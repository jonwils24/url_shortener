class ShortenedUrl < ActiveRecord::Base
  validates :short_url, presence: true, uniqueness: true
  validates :long_url, presence: true
  validates :submitter_id, presence: true
  
  validates_length_of :long_url, maximum: 1024
  
  validate :less_than_six_in_a_minute

  belongs_to(
    :submitter,
    class_name: "User",
    foreign_key: :submitter_id,
    primary_key: :id
  )
  
  has_many(
    :visits,
    class_name: "Visit",
    foreign_key: :shortened_url_id,
    primary_key: :id
  )
  
  has_many(
    :visitors,
    Proc.new { distinct },
    through: :visits,
    source: :visitor
  )
  
  def self.random_code
    string = ""
    
    begin
      string = SecureRandom.urlsafe_base64
    end while ShortenedUrl.exists?(short_url: string)
    
    string
  end
  
  def self.create_for_user_and_long_url!(user, long_url)
    code = self.random_code
    
    self.create!(submitter_id: user.id, short_url: code, long_url: long_url)
  end
  
  def num_clicks
    visits.count
  end
  
  def num_uniques
    visitors.count
  end
  
  def num_recent_uniques(since = 10.minutes.ago)
    visits.where(updated_at: since..Time.now).distinct.count
  end
  
  private
  
  def less_than_six_in_a_minute
    submitted = ShortenedUrl
      .where(submitter_id: submitter_id)
      .where("updated_at > ?", 1.minute.ago)
      .count
  
    if submitted > 5
      errors[:submitter_id] << "can't submit more than five urls in a minute"
    end
  end
end

