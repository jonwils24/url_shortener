class ChangeConstraints < ActiveRecord::Migration
  def change
    change_column_null :shortened_urls, :long_url, false
    change_column_null :shortened_urls, :short_url, false
    change_column_null :shortened_urls, :submitter_id, false
    
    change_column_null :tag_topics, :category, false
    
    change_column_null :taggings, :shortened_url_id, false
    change_column_null :taggings, :tag_topic_id, false
    
    change_column_null :users, :email, false
    
    change_column_null :visits, :user_id, false
    change_column_null :visits, :shortened_url_id, false
  end
end
