class AddIndex < ActiveRecord::Migration
  def change
    add_index :taggings, :shortened_url_id
    add_index :taggings, :tag_topic_id
    add_index :taggings, [:shortened_url_id, :tag_topic_id], unique: true
  end
end
