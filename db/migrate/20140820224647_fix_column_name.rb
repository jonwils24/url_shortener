class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :taggings, :tagtopic_id, :tag_topic_id
  end
end
